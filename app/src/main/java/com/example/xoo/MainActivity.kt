package com.example.xoo

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    private lateinit var button1:Button
    private lateinit var button2:Button
    private lateinit var button3:Button
    private lateinit var button4:Button
    private lateinit var button5:Button
    private lateinit var button6:Button
    private lateinit var button7:Button
    private lateinit var button8:Button
    private lateinit var button9:Button
    private var activePlayer = 1
    private var firstPlayer = ArrayList<Int>()
    private var secondPlayer = ArrayList<Int>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
    private fun init(){

        button1 = findViewById(R.id.Bt1)
        button2 = findViewById(R.id.Bt2)
        button3 = findViewById(R.id.Bt3)
        button4 = findViewById(R.id.Bt4)
        button5 = findViewById(R.id.Bt5)
        button6 = findViewById(R.id.Bt6)
        button7 = findViewById(R.id.Bt7)
        button8 = findViewById(R.id.Bt8)
        button9 = findViewById(R.id.Bt9)
        button1.setOnClickListener(this)
        button2.setOnClickListener(this)
        button3.setOnClickListener(this)
        button4.setOnClickListener(this)
        button5.setOnClickListener(this)
        button6.setOnClickListener(this)
        button7.setOnClickListener(this)
        button8.setOnClickListener(this)
        button9.setOnClickListener(this)
    }

    fun onClick(clickedView: View?) {
        if (clickedView is View){
            var buttonNumber = 0
            when (clickedView.id) {
                R.id.Bt1 -> buttonNumber = 1
                R.id.Bt2 -> buttonNumber = 2
                R.id.Bt3 -> buttonNumber = 3
                R.id.Bt4 -> buttonNumber = 4
                R.id.Bt5 -> buttonNumber = 5
                R.id.Bt6 -> buttonNumber = 6
                R.id.Bt7 -> buttonNumber = 7
                R.id.Bt8 -> buttonNumber = 8
                R.id.Bt9 -> buttonNumber = 9
            }
            if (buttonNumber!=0){
                playGame(clickedView as Button, buttonNumber)
            }
        }
    }

    private fun playGame(clickedView: Button, buttonNumber: Int) {
        if (activePlayer==1){
            clickedView.text = "X"
            clickedView.setBackgroundColor(Color.RED)
            activePlayer = 2
            firstPlayer.add(buttonNumber)
        }else {
            secondPlayer.add(buttonNumber)
            clickedView.text = "0"
            clickedView.setBackgroundColor(Color.YELLOW)
            activePlayer=1
        }
        clickedView.isEnabled = false
        check()
    }
    private fun check(){
        var winnerPlayer = 0
        if (firstPlayer.contains(1) && firstPlayer.contains(2) && firstPlayer.contains(3)){
            winnerPlayer = 1
        }
        if (secondPlayer.contains(1) && secondPlayer.contains(2) && secondPlayer.contains(3)){
            winnerPlayer = 2
        }
        if (firstPlayer.contains(4) && firstPlayer.contains(5) && firstPlayer.contains(6)){
            winnerPlayer = 1
        }
        if (secondPlayer.contains(4) && secondPlayer.contains(5) && secondPlayer.contains(6)){
            winnerPlayer = 2
        }
        if (firstPlayer.contains(7) && firstPlayer.contains(8) && firstPlayer.contains(9)){
            winnerPlayer = 1
        }
        if (secondPlayer.contains(7) && secondPlayer.contains(8) && secondPlayer.contains(9)){
            winnerPlayer = 2
        }
        if (firstPlayer.contains(1) && firstPlayer.contains(4) && firstPlayer.contains(7)){
            winnerPlayer = 1
        }
        if (secondPlayer.contains(1) && secondPlayer.contains(4) && secondPlayer.contains(7)){
            winnerPlayer = 2
        }
        if (firstPlayer.contains(2) && firstPlayer.contains(5) && firstPlayer.contains(8)){
            winnerPlayer = 1
        }
        if (secondPlayer.contains(2) && secondPlayer.contains(5) && secondPlayer.contains(8)){
            winnerPlayer = 2
        }
        if (firstPlayer.contains(3) && firstPlayer.contains(6) && firstPlayer.contains(9)){
            winnerPlayer = 1
        }
        if (secondPlayer.contains(3) && secondPlayer.contains(6) && secondPlayer.contains(9)){
            winnerPlayer = 2
        }
        if (firstPlayer.contains(1) && firstPlayer.contains(5) && firstPlayer.contains(9)){
            winnerPlayer = 1
        }
        if (secondPlayer.contains(1) && secondPlayer.contains(5) && secondPlayer.contains(9)){
            winnerPlayer = 2
        }
        if (firstPlayer.contains(3) && firstPlayer.contains(5) && firstPlayer.contains(7)){
            winnerPlayer = 1
        }
        if (secondPlayer.contains(3) && secondPlayer.contains(5) && secondPlayer.contains(7)){
            winnerPlayer = 2
        }
        if (winnerPlayer == 1)
            Toast.makeText(this, "first player won", Toast.LENGTH_LONG).show()
        else if (winnerPlayer == 2)
            Toast.makeText(this , "second player won" , Toast.LENGTH_LONG).show()
    }
}

private fun Button.setOnClickListener(mainActivity: MainActivity) {

}
